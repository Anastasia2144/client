import com.google.gson.Gson;
import communication.Connection;
import communication.Request;
import communication.RequestType;
import communication.Response;
import util.ServerAddress;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Scanner;

public class Client {
    private final static Gson gson = new Gson();

    public static void main(String[] args) {
        try {
            ServerAddress address = gson.fromJson(new FileReader(new File("src/main/resources/socket.json")), ServerAddress.class);

            try (Socket socketServer = new Socket(address.getHost(), address.getPort());) {

                System.out.println("Соединение с сервером установлено.");

                Connection connection = new Connection(socketServer);


                System.out.println("Для работы со словарем доступны следующие операции: \n" +
                        "CREATE - добавить в словарь новое слово; \n" +
                        "UPDATE - обновить значение слова; \n" +
                        "FIND - найти значение слова; \n" +
                        "FIND_MASK - найти значения слов по маске; \n" +
                        "DELETE - удалить слово из словаря; \n" +
                        "Для того, чтобы закончить работу со словарем введите EXIT.");

                while (true) {
                    Request request = formRequest();

                    if (request.getType().equals(RequestType.EXIT)) {
                        connection.send(gson.toJson(request));
                        break;
                    }

                    connection.send(gson.toJson(request));

                    Response response = gson.fromJson(connection.receive(), Response.class);

                    Map<String, String> result = response.getResult();

                    for (Map.Entry<String, String> pair : result.entrySet()) {
                        System.out.println(pair.getKey() + ": " + pair.getValue());
                    }
                }

                connection.close();

                System.out.println("Клиент отключен.");

            } catch (IOException e) {
                System.out.println("Сервер недоступен.");

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Конфигурационный файл соеденения не найден");
        }
    }

    private static Request formRequest() {
        Request request = new Request();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите название операции:");

        while (true) {
            try {
                String type = scanner.nextLine();
                request.setType(RequestType.valueOf(type));
                break;
            } catch (IllegalArgumentException e) {
                System.out.println("Такой операции не существует.");
            }
        }

        if (request.getType().equals(RequestType.EXIT)) {
            return request;
        }

        String str = "слово";
        if(request.getType().equals(RequestType.FIND_MASK))
            str = "маску";

        System.out.format("Введите %s: \n", str);

        String word = scanner.nextLine();

        while (word.equals("")) {
            System.out.format("Введите еще раз %s. \n", str);
            word = scanner.nextLine();
        }

        request.setWord(word);

        if (request.getType().equals(RequestType.CREATE) || request.getType().equals(RequestType.UPDATE)) {
            System.out.println("Введите значение слова");
            String meaning = scanner.nextLine();

            while (meaning.equals("")) {
                System.out.println("Введите еще раз значение слова");
                meaning = scanner.nextLine();
            }

            request.setMeaning(meaning);
        }

        return request;
    }
}
