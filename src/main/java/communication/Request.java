package communication;

import java.io.Serializable;

public class Request implements Serializable {

    private RequestType type;
    private String word;
    private String meaning;

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        word = word.toLowerCase();
        this.word = word;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        meaning = meaning.toLowerCase();
        this.meaning = meaning;
    }


}
