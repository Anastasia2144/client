package communication;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Connection implements Closeable {
    private final Socket socket;
    private final ObjectOutputStream out;
    private final ObjectInputStream in;

    public Connection(Socket socket) throws IOException {
        this.socket = socket;
        this.out = new ObjectOutputStream(socket.getOutputStream());
        this.in = new ObjectInputStream(socket.getInputStream());
    }

    public void send(String stringRequest) throws IOException {
        out.writeObject(stringRequest);
    }

    public String receive() throws IOException, ClassNotFoundException {
        return in.readObject().toString();
    }

    public void close() throws IOException {
        out.close();
        in.close();
        socket.close();
    }
}
